const config = require("./config.json");
const fetch = require("node-fetch");

const SocketServer = require('ws').Server;
const express = require("express");
var app = express();
var path = require('path');

// We use ForkAwesome and MaterialDesign fonts so tofus are quite normal
const protocolList = {
  "discord": {
    "icon": '',
    "color": '#5865f2',
  },
  "gitter": {},
  "irc": {},
  "keybase": {
    "icon": ''
  },
  "mattermost": {
    "color": '#0058cc'
  },
  "matrix": {
    "font": '',
    "color": '#0dbd8b'
  },
  "msteams": {
    "color": '#464eb8'
  },
  "mumble": {
    "icon": '󰋎',
    "font": 'MaterialDesign'
  },
  "nctalk": {
    "icon": '',
    "color": '#0082c9'
  },
  "rocketchat": {
    "icon": '',
    "color": 'f5445c'
  },
  "slack": {
    "icon": '',
    "color": '#4a154b'
  },
  "sshchat": {
    "icon": ''
  },
  "telegram": {
    "icon": '',
    "color": '#0088cc'
  },
  "vk": {
    "icon": '',
    "color": '#4376a6'
  },
  "whatsapp": {
    "icon": '',
    "color": '#25d366'
  },
  "xmpp": {
    "icon": '',
    "color": '#333333'
  },
  "zulip": {}
};
const specialAccountList = {
  "irc.twitch": {
    "icon": '',
    "color": '#6441a4'
  },
  "xmpp.peertube": {
    "icon": '',
    "color": '#f16805'
  }
};
const userColor = [
  '#ff7e7e',
  '#95ff95',
  '#ffff90',
  '#a6ffff',
  '#9eceff',
  '#ff91ff',
  '#ffcb7e'
];
const nodeIcon = {
  "icon": '',
  "color": '#68a063',
  "font": 'ForkAwesome'
};
var nickColorList = {};
var currentColor = 0;
var connectionFailed = false;

app.use('/fonts', express.static('fonts'));
app.use('/js', express.static('js'));

const port = process.env.PORT || 1314;
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname + '/index.html'));
});
var server = app.listen(port, function () {
  console.log('node.js static server listening on port: ' + port)
});

const wss = new SocketServer({ server });

// Init Websocket ws and handle incoming connect requests
wss.on('connection', function connection(ws) {
  console.log("connection ...");
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
  });
});

function getUrl() {
  if (config.matterbridge_url){
    return config.matterbridge_url;
  }
  return 'http://localhost:4242';
}

function getHeaders() {
  if (typeof config.token === 'undefined') {
    return {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
  } else { // If a security token is set
    return {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + config.token
    }
  }
}

function manageMessages (response) {
  if (connectionFailed === true) {
    sendStatus('Connection with Matterbridge restored');
    connectionFailed = false;
  }
  response.json()
  .then(
    data => {
      if (data.length > 0) {
        data.sort((a, b) => {
          return new Date(a.timestamp) - new Date(b.timestamp);
        });
        data.forEach(message => formatMessage(message));
      }/* else {
        console.log('No message in Matterbridge buffer');
      }*/
    }
  )
  .catch(err => {
      console.log(err);
      sendError('Error in message management');
    }
  )

}

function deliverMessages() {
    fetch(getUrl() + '/api/messages', {
      method: 'GET',
      headers: getHeaders()
    }).then(manageMessages)
    .catch(err => {
      console.log('ERROR fetch: %s', err.message);
      if (connectionFailed === false) { // Just send the info once
        sendError('Matterbridge seems to be unreachable');
      }
      connectionFailed = true;
    });
}

function serviceIcon(account, protocol) {
  // Check if a custom icon for this account was set in the config file
  if (config.custom_icons && config.custom_icons[account] && config.custom_icons[account].icon) {
    return config.custom_icons[account].icon
  }

  // Check for an icon for this account was harcoded
  if (specialAccountList[account] && specialAccountList[account].icon) {
    return specialAccountList[account].icon
  }

  // Check if a custom icon for this protocol was set in the config file
  if (config.custom_icons && config.custom_icons[protocol] && config.custom_icons[protocol].icon) {
    return config.custom_icons[protocol].icon
  }

  // Check for an icon for this protocol was harcoded
  if (protocolList[protocol] && protocolList[protocol].icon) {
    return protocolList[protocol].icon
  }

  // If nothing (We use ForkAwesome so the tofu is quite normal)
  return ''
}

function serviceColor(account, protocol) {
  // Check if a custom icon color for this account was set in the config file
  if (config.custom_icons && config.custom_icons[account] && config.custom_icons[account].color) {
    return config.custom_icons[account].color
  }

  // Check for an icon color for this account was harcoded
  if (specialAccountList[account] && specialAccountList[account].color) {
    return specialAccountList[account].color
  }

  // Check if a custom icon color for this account was set in the config file
  if (config.custom_icons && config.custom_icons[protocol] && config.custom_icons[protocol].color) {
    return config.custom_icons[protocol].color
  }

  // Check for an icon color for this protocol was harcoded
  if (protocolList[protocol] && protocolList[protocol].color) {
    return protocolList[protocol].color
  }

  // If nothing, white
  return '#ffffff'
}

function serviceFont(account, protocol) {
  // Check if a custom icon font for this account was set in the config file
  if (config.custom_icons && config.custom_icons[account] && config.custom_icons[account].font) {
    return config.custom_icons[account].font
  }

  // Check for an icon font for this account was harcoded
  if (specialAccountList[account] && specialAccountList[account].font) {
    return specialAccountList[account].font
  }

  // Check if a custom icon font for this account was set in the config file
  if (config.custom_icons && config.custom_icons[protocol] && config.custom_icons[protocol].font) {
    return config.custom_icons[protocol].font
  }

  // Check for an font color for this protocol was harcoded
  if (protocolList[protocol] && protocolList[protocol].font) {
    return protocolList[protocol].font
  }

  // If no customisation, ForkAwesome is the default
  return 'ForkAwesome'
}

function nextColor() {
  if (currentColor > userColor.length) {
    currentColor = 0;
    // console.log(currentColor + ' reset');
  } else {
    currentColor++;
    // console.log(currentColor + ' increment');
  }
}

function sendMessage(payload) {
  wss.clients.forEach((client) => {
    if (client.readyState === 1) {
      client.send(JSON.stringify(payload));
    }
  });
}

function sendStatus(message) {
  sendMessage({
    "type": 'info',
    "service": nodeIcon,
    "user": {
      "name": 'INFO',
      "color": '#00ff00',
    },
    "content": message
  });
  console.log('INFO: %s', message);
}

function sendError(message) {
  sendMessage({
    "type": 'info',
    "service": nodeIcon,
    "user": {
      "name": 'ERROR',
      "color": '#ff0000',
    },
    "content": message
  });
  console.log('ERROR: %s', message);
}

function formatMessage(message) {
  // console.log(message);

  const userId = message.username + '@' + message.account;
  if (nickColorList[userId] === undefined) {
    nextColor();
    nickColorList[userId] = userColor[currentColor];
    console.log(Object.keys(nickColorList).length + ' users\' color cached');
  }

  let payload = {
    "type": 'message',
    "gateway": message.gateway,
    "service": {
      "icon": serviceIcon(message.account, message.protocol),
      "color": serviceColor(message.account, message.protocol),
      "font": serviceFont(message.account, message.protocol)
    },
    "user": {
      "name": message.username,
      "color": nickColorList[userId]
    },
    "content": message.text
      .replace(/\n+/g, '<br>')
      .replace(/<(:[^:]*:)[0-9]*>/g, '$1'),
    /*"timestamp": new Date(message.timestamp)
      .toLocaleTimeString("fr-FR", {
        timezone: "Europe/Paris",
        hour: "2-digit",
        minute: "2-digit"
      })*/
  }
  sendMessage(payload);
  console.log('Message from %s sent', message.account);
}

setInterval(deliverMessages, 3000);

function keepClientsAlive() {
  sendMessage({ "type": "heartbeat" });
}
setInterval(keepClientsAlive, 20000);
