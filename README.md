# OBS Matterbridge Overlay
This is just an overlay using Matterbirdge to be used with OBS.
It can provide an overlay per Matterbridge gateway. So it can be used for multiple users.

Completely inspired and based on [obs-discord-overlay](https://gitlab.com/refrac/obs-discord-overlay) made by @ozmose

## *Warning*
**The actual docker-compose file is the same as [obs-discord-overlay](https://gitlab.com/refrac/obs-discord-overlay), so it may not work since it use Matterbridge**

## *Reminder*
Since this tool is design to be used on OBS with multiple stream services, reminder about some services will be provided.

### [*Matterbridge*](documentation/matterbridge.md#reminder)

### [*PeerTube instances*](documentation/peertube.md#reminder)

### *Twitch*
Matterbridge is using IRC for Twitch chat. Since it does not use directly Twitch API:
- Matterbridge does not interpret emotes name.
- **Do not use bridges to send other services message to Twitch, there is [limitations](https://dev.twitch.tv/docs/irc/guide#rate-limits).**
- If you still use bidirectional messages, note that messages send by the account used for Matterbridge will be ignored by Matterbridge, so the overlay will not show them.

### *OBS browser sources*
If you use OBS on Linux:
- Pop!_OS, Linux Mint, Ubuntu user, please use the PPA.
- Manjaro, ArchLinux user please use `obs-studio-tytan652` AUR recipe.
- Or use the Flatpak package.

Why ? To have the browser source available.

## [Matterbridge](documentation/matterbridge.md)

## [Installation](documentation/installation.md)

## [Use in OBS](documentation/use_in_obs.md)

## But but but ... Whyyyy...

### ... it exist ?
I wanted to help someone by creating an overlay using both PeerTube and Twitch.
### ... Matterbridge ?
I wanted to be able to show a chat with many protocol and Matterbridge can allow that.
### ... MIT License ?
Because free software is always better than proprietary software. But Since it can be a SaaS maybe jump to AGPL, I don't know.
### ... no YouTube/Facebook Live/Whatever service chat ?
Because Matterbridge does not support it.

## Terms of Use
This code is copyright © tytan652 and available under the terms of the MIT license.

## Fonts
- [FiraCode](https://github.com/tonsky/FiraCode) for nickname and message.
- [ForkAwesome](https://forkaweso.me/Fork-Awesome/) for provider icons. Thank you @Booteille for the discovery.
- [MaterialDesign Webfont](https://materialdesignicons.com/) for custom icons. Seriously !? It provides a duck and we need it.

## Client side JS library
[ReconnectingWebSocket](https://github.com/joewalnes/reconnecting-websocket) under MIT license is used to make the WebSocket client able to reconnect if the server is restarted.
