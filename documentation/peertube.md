# PeerTube instances
**For built-in Prosody mode only**, other mode may work but do not need to follow this guide.

## *Reminder*
**You will need to change the room for each live and restart Matterbridge. Restarting Matterbridge will skip messages if some users are actually streaming.**

**So provide/use this tool only you can avoid this issue with some organisation between users.**

## Requirements
- [PeerTube plugin livechat](https://github.com/JohnXLivingston/peertube-plugin-livechat) version 3.2.0 or later.
- [Matterbridge](https://github.com/42wim/matterbridge) version 1.22.4 or later.

Your PeerTube instance and Matterbridge will need to run on the same machine. Exception are mentioned later.

## Livechat (admins only)
You will need to enable the new advanced option added in 3.2.0 for built-in Prosody: **`Enable client to server connections`**.

This will allow localhost XMPP clients to connect to the Prosody server.

You may need to add some line to your `/etc/hosts`:

```
127.0.0.1       anon.example.xyz room.example.xyz
```
Replace `example.xyz` by your actual instance domain name.

*Except if you know about how XMPP server work and decide to set everything up to open connections from non-local client. So streamer can self-host their overlay themselves.*

## Matterbridge
Installation [here](matterbridge.md).

In the version 1.22.4, Matterbridge now support XMPP anonymous connection needed to connect to the built-in prosody.

So in the TOML config file put:

``` TOML
[xmpp.mypeertube]
Anonymous=true
Server="anon.example.xyz:52822"
Muc="room.example.xyz"
Nick="Matterbridge"
RemoteNickFormat="[{PROTOCOL}] <{NICK}> "
NoTLS=true
```
- Replace `example.xyz` by your actual instance domain name.
- Replace `52800` by the actual port if you changed it.
- `mypeertube` can be replaced by another name.
- Using `peertube` will provide put PeerTube icon for overlay messages, can be also done with overlay config modification.
- As of the version 7 of the livechat plugin, some issue with TLS appeared, adding `NoTLS=true` seems to fix. But I don't have time to dig more into it.

Now you can add this account to gateways.
