# Use in OBS
- Add a new browser source with at least a gateway set.
- **Set the width and height**, do not change the size in the preview.

## URL customisation
- If you use this locally, `http://localhost:1314/` is your base URL.
- If you use this with a server, `https://overlay.example.xyz/` is how your base URL should look.

### `gateway`
This indicate which gateway you want to use, only one is supported.

> Example with a gateway named `mygateway`: `http://localhost:1314/?gateway=mygateway`

If not added, it will not even try to connect to the overlay server.

### Optional
#### `prefix`
*True by default.*

This indicate if you want to show the prefix showing from which protocol/account comes from the messages.

>Default: `true`

>Example: `http://localhost:1314/?gateway=mygateway&prefix=false`

#### `verbose`
This indicate if you want info messages (WebSocket disconnection or Matterbridge unreachable) to be shown in the overlay.
You will only see them if the Matterbridge or if the overlay server is down or seems to be.

>Default: `true`

>Example: `http://localhost:1314/?gateway=mygateway&verbose=false&prefix=false`
