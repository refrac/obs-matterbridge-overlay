# Configuration
The project need a file named `config.json`.

Example:
```jsonc
{
  "matterbridge_url": "http://localhost:4242",
  /* OPTIONAL */
  "token": "",
  "custom_icons": {
    "xmpp": {
      "icon": "ﳽ" // Custom default icon/text for this protocol
    },
    "xmpp.myxmpp": { // Name of an account
      "icon": "", // Custom icon/text for this account
      "color": "#fff000" // Custom icon color for this account
    },
    "irc.twitch": {
        "icon": "󰕃",
        "font": "MaterialDesign" // Custom icon font for this account
    }
  }
}
```

But it can be empty like this:
```json
{}
```
And it will use only default config.

## `matterbridge_url`
The URL to the Matterbridge API with the port.

> Default: `http://localhost:4242`

## Optional
### `token`
The security token of the Matterbridge API if you put one.

### `custom_icons`
We don't use image but icon present in fonts to make you able to use text flawlessly.

Those icons indicate from which account or protocol comes the message.

If you see tofu (little rectangle) in the code or config file this is because we use font which add icons that are not in the font that you are actually using.

You can customise the icon/text (yes you can put text if you want) showed before the nickname and also its color.

By adding an object named with the protocol or account that you want to customise. And put inside the icon/test and color that you want.

#### Default
Some Matterbridge protocol have already a icon and color preset (except Gitter, IRC and zulip).

Some account name like `irc.twitch` and `xmpp.peertube` have also a preset.

If none, it fallback to a pretty standard white message bubble icon.

#### `icon`
>Default depend on the protocol and maybe on the account name.

>Example: ``, `PeerTube`

#### `color`
Put the color is set with hex code that you want.

>Default depend on the protocol and maybe on the account name.

>Example: `#ffffff`

#### `font`
There are three available fonts:

- `ForkAwesome` for default and custom icons ([Cheatsheet](https://forkaweso.me/Fork-Awesome/icons/)).
- `MaterialDesign` for more custom icons ([Cheatsheet](https://pictogrammers.github.io/@mdi/font/5.8.55/)).
- `FiraCode` to put text rather than an icon.

- If you type it wrong, CSS will fallback to monospace.

>Default: `ForkAwesome`
