# Installation
**Only tested with nodeJS 14.**
Install nodeJS 14 or later with the method you like.

## Locally
[Firstly, install Matterbridge](matterbridge.md#locally).

Download the latest version of the overlay [here](https://gitlab.com/refrac/obs-matterbridge-overlay/-/releases) and extract it.

Or clone it if you want to use the master branch:
```sh
git clone https://gitlab.com/refrac/obs-matterbridge-overlay.git
```

Move inside the folder and install npm modules:
```sh
npm install
```

Copy the default config file:
```sh
cp config.default.json config.json
```

And put the right URL to Matterbridge inside it.

And start the overlay:
```sh
npm start
```

## Server (Linux instructions only)
[Firstly, install Matterbridge](matterbridge.md#server).

Create a `overlay` user with `/var/www/overlay` as home:
```sh
sudo useradd -m -d /var/www/overlay -s /bin/bash -p overlay overlay
```

Set its password:
```sh
sudo passwd overlay
```

Log in as `overlay`:
```sh
sudo -u overlay -i
```

Download the latest version of the overlay [here](https://gitlab.com/refrac/obs-matterbridge-overlay/-/releases) and extract it.

Or clone it if you want to use git:
```sh
git clone https://gitlab.com/refrac/obs-matterbridge-overlay.git
```

Move inside and install npm modules:
```sh
cd obs-matterbridge-overlay
npm install
```

Copy the default config file:
```sh
cp config.default.json config.json
```
And put the right URL to Matterbridge inside it.

### Add systemd service
Copy the `obs-matterbridge-overlay.service` of this repo to `/etc/systemd/system`:
```sh
sudo cp obs-matterbridge-overlay/distrib/systemd/obs-matterbridge-overlay.service /etc/systemd/system/
```

**You need to change the nodeJS path inside to the one you want to use.**

Then you can edit it at your needs.

Tell systemd to reload its config:
```sh
sudo systemctl daemon-reload
```

Test if it launch:
```sh
sudo systemctl start obs-matterbridge-overlay
sudo journalctl -feu obs-matterbridge-overlay
```

And then you can enable it:
```sh
sudo systemctl enable matterbridge
```

### Web access
To use the overlay you will need to configure your HTTP server with HTTPS and Websocket (`wss://`) access to the overlay.

#### Nginx
An example of config is available in `distrib/nginx` folder.
