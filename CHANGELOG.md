# Changelog
## 0.1.1
### Features
- Add PeerTube icon preset

### Fixes
- Switch Default icon to ForkAwesome (before was NerdFont)

## 0.1.0
Initial release