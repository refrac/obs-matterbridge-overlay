# Matterbridge

## *Reminder*
- If you change the config meanwhile Matterbridge is running, you will need to restart it but this will skip messages so do it only when nobody use it.

- If you are using Matterbridge with a Twitch chat, I recommend you to add `Charset="utf-8"` to his IRC configuration.
Why ? Because charset auto-detection can failed and some letter end up replaced by the wrong character.

## Installation
Download the latest version [here](https://github.com/42wim/matterbridge/releases/latest).

### Locally
Put it in a folder if you want and create a TOML file named `matterbridge.toml` in the same folder as the Matterbridge binary.

And once your configuration is done, you can launch it in a terminal if possible.

### Server (Linux instructions only)
Move it to `/usr/local/bin`:
```sh
sudo cp matterbridge-1.22.4-linux-64bit /usr/local/bin/matterbridge
```
The name of the binary can be different.

Create your Matterbridge config file inside `/etc`:
```sh
sudo mkdir /etc/matterbridge
sudo touch /etc/matterbridge/bridge.toml
```

### Add systemd service
Create the user `matterbridge` (no home needed):
```sh
sudo useradd -M -s /bin/bash -p matterbridge matterbridge
```

Set its password:
```sh
sudo passwd matterbridge
```

Copy the `matterbridge.service` of this repo to `/etc/systemd/system`:
```sh
sudo cp obs-matterbridge-overlay/distrib/systemd/matterbridge.service /etc/systemd/system/
```

You can edit it at your needs.

Tell systemd to reload its config:
```sh
sudo systemctl daemon-reload
```

And once your Matterbridge configuration is done, you can launch it with systemd.

Test your config first:
```sh
sudo systemctl start matterbridge
sudo journalctl -feu matterbridge
```

And then you can enable it:
```sh
sudo systemctl enable matterbridge
```

## Configuration
*Reminder:*
- Do not always use `gateway.inout`. `gateway.in` (receive only) `gateway.out` (emit only) exist.
- Avoid bidirectional communication between chat services in gateways, use `gateway.in` rather than `gateway.inout` if a service have limitations.
- For the API, use only `gateway.out`, The `in` is useless in this situation.
- Also for API, put `"{NICK}"` as `RemoteNickFormat` for the API because the overlay will show the protocol an other way.
- Use only one account by service type and use them in any gateways.
- Create a gateway per user.


Use the following pages:
- [Configuration page](https://github.com/42wim/matterbridge#configuration) on the Matterbridge repo.
- [API wiki page](https://github.com/42wim/matterbridge/wiki/API) on the Matterbridge repo.
